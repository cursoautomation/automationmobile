package autocourse.Tests;

import com.autocourse.Pages.homePage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class testMobile {
    AppiumDriverLocalService m_service;
    AndroidDriver m_driver;

    @Before
    public void start() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        String udid = "ZY3224DLVV";//
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, udid);
        capabilities.setCapability(MobileCapabilityType.UDID, udid);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "5.1");
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
        //capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.android.launcher");
        //capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.android.launcher2.Launcher");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.android.launcher3");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".CustomizationPanelLauncher");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, true);

        URL remoteUrl = new URL("http://localhost:4723/wd/hub");
        m_driver = new AndroidDriver(remoteUrl, capabilities);


    }

    @Test
    public void testMobile() throws Exception{
        homePage hp = new homePage(m_driver);
        hp.clickChrome();
        assertTrue(true);


    }

    @After
    public  void afterMethod() {
        m_driver.quit();
    }
}
